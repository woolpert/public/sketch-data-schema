{
  "$id": "data.schema.json",
  "$schema": "https://json-schema.org/draft/2020-12/schema",
  "title": "Sketch Data",
  "description": "A format for transmitting sketches used in the computer-assisted mass appraisal industry. See https://www.opencamadata.org/standards/sketch for details.",
  "type": "object",
  "properties": {
    "$schema": {
      "title": "Schema URI",
      "description": "URI to the specific version of the SDS used by this document",
      "type": "string"
    },
    "unitOfMeasure": {
      "$ref": "#/definitions/unitOfMeasure"
    },
    "lookupCollection": {
      "title": "Lookup collection",
      "description": "Dictionary of lookup data (`lookupData`) identified by `lookupDataId`.  The root of a hierarchical dictionary of `lookup` values indexed by `[lookupDataId][domain][lookupId]` used to associate elements of the sketch with metadata and styles.",
      "type": "object",
      "additionalProperties": {
        "$ref": "#/definitions/lookupData"
      }
    },
    "sketches": {
      "type": "array",
      "items": {
        "$ref": "#/definitions/sketch"
      }
    }
  },
  "required": [
    "$schema",
    "sketches"
  ],
  "additionalProperties": false,
  "definitions": {
    "unitOfMeasure": {
      "title": "Unit of measure",
      "type": "string",
      "enum": [
        "feet",
        "meters"
      ],
      "default": "feet"
    },
    "domain": {
      "title": "Domain",
      "description": "A domain is a broad term for a collection of segment types or lookup types. For example, 'outbuildings' are a whole class of things that belong approximately in the same 'domain'.",
      "enum": [
        "unspecified",
        "outbuilding"
      ],
      "type": "string"
    },
    "rgbHexColor": {
      "title": "RGB color in hexadecimal",
      "description": "A three or six character representation of an RGB value in hexadecimal.",
      "type": "string",
      "pattern": "^#([\\da-fA-F]{3}){1,2}$",
      "examples": [
        "#ffffff",
        "#fff"
      ]
    },
    "color": {
      "title": "color",
      "oneOf": [
        {
          "$ref": "#/definitions/rgbHexColor"
        },
        {
          "$ref": "#/definitions/noColor"
        }
      ]
    },
    "noColor": {
      "title": "No color",
      "type": "string",
      "const": "none"
    },
    "stroke": {
      "title": "stroke",
      "oneOf": [
        {
          "$ref": "#/definitions/rgbHexColor"
        },
        {
          "$ref": "#/definitions/noStroke"
        }
      ]
    },
    "strokeWidth": {
      "title": "strokeWidth",
      "type": "integer",
      "default": 1
    },
    "noStroke": {
      "title": "No stroke",
      "type": "string",
      "const": "none"
    },
    "fill": {
      "title": "fill",
      "oneOf": [
        {
          "$ref": "#/definitions/rgbHexColor"
        },
        {
          "$ref": "#/definitions/noFill"
        }
      ]
    },
    "noFill": {
      "title": "No fill",
      "type": "string",
      "const": "none"
    },
    "strokeDashArray": {
      "title": "strokeDashArray",
      "description": "Follows the [SVG standard](https://developer.mozilla.org/en-US/docs/Web/SVG/Attribute/stroke-dasharray) for defining dashed line patterns.",
      "type": "array",
      "items": {
        "type": "integer"
      }
    },
    "fontFamily": {
      "title": "fontFamily",
      "type": "string",
      "examples": [
        "Georgia, serif"
      ]
    },
    "fontWeightText": {
      "title": "fontWeightText",
      "enum": [
        "thin",
        "normal",
        "bold"
      ],
      "type": "string"
    },
    "fontWeightNumber": {
      "title": "fontWeightNumber",
      "enum": [
        100,
        200,
        300,
        400,
        500,
        600,
        700,
        800
      ],
      "type": "number"
    },
    "fontWeight": {
      "title": "fontWeight",
      "oneOf": [
        {
          "$ref": "#/definitions/fontWeightText"
        },
        {
          "$ref": "#/definitions/fontWeightNumber"
        }
      ]
    },
    "fontSize": {
      "title": "fontSize",
      "type": "number",
      "minimum": 0
    },
    "fontStyle": {
      "title": "fontStyle",
      "enum": [
        "normal",
        "italic"
      ],
      "type": "string"
    },
    "style": {
      "type": "object",
      "title": "Style",
      "properties": {
        "color": {
          "$ref": "#/definitions/color"
        },
        "fontFamily": {
          "$ref": "#/definitions/fontFamily"
        },
        "fontSize": {
          "$ref": "#/definitions/fontSize"
        },
        "fontStyle": {
          "$ref": "#/definitions/fontStyle"
        },
        "fontWeight": {
          "$ref": "#/definitions/fontWeight"
        },
        "stroke": {
          "$ref": "#/definitions/stroke"
        },
        "strokeWidth": {
          "$ref": "#/definitions/strokeWidth"
        },
        "strokeDashArray": {
          "$ref": "#/definitions/strokeDashArray"
        },
        "fill": {
          "$ref": "#/definitions/fill"
        },
        "fillPattern": {
          "$ref": "#/definitions/fillPattern"
        }
      },
      "additionalProperties": false
    },
    "pathElement": {
      "type": "object",
      "title": "pathElement",
      "properties": {
        "path": {
          "type": "string",
          "title": "Drawing path",
          "description": "Defines a sequence of path commands to draw.  This definition follows the [SVG standard](https://svgwg.org/svg2-draft/paths.html#PathData) for path data.",
          "examples": [
            "M100,50 h-150 a150,150 0 1,0 150,-150 z",
            "M200,75 l 50,-25 a25,25 -30 0,1 50,-25 l 50,-25"
          ]
        },
        "style": {
          "$ref": "#/definitions/style"
        }
      },
      "required": [
        "path"
      ],
      "additionalProperties": false
    },
    "element": {
      "type": "object",
      "title": "element",
      "oneOf": [
        {
          "$ref": "#/definitions/pathElement"
        }
      ]
    },
    "pattern": {
      "title": "Pattern",
      "description": "Defines a pattern using the specified elements and style.",
      "type": "object",
      "properties": {
        "height": {
          "type": "integer",
          "minimum": 0
        },
        "width": {
          "type": "integer",
          "minimum": 0
        },
        "elements": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/element"
          }
        }
      },
      "additionalProperties": false
    },
    "noPattern": {
      "title": "No pattern",
      "type": "string",
      "const": "none"
    },
    "fillPattern": {
      "title": "Fill pattern",
      "oneOf": [
        {
          "$ref": "#/definitions/pattern"
        },
        {
          "$ref": "#/definitions/noPattern"
        }
      ]
    },
    "attributes": {
      "type": "array",
      "title": "Attributes",
      "description": "Generic collection of zero or more attributes with a key and value",
      "items": {
        "type": "object",
        "properties": {
          "key": {
            "type": "string",
            "minLength": 1
          },
          "value": {
            "type": [
              "string",
              "number",
              "object",
              "boolean",
              "array"
            ]
          }
        },
        "required": [
          "key",
          "value"
        ],
        "additionalProperties": false
      }
    },
    "lookupDataId": {
      "title": "Lookup Data ID",
      "description": "A unique identifier for lookup data (`lookupData`).",
      "minLength": 1,
      "maxLength": 20,
      "type": "string",
      "examples": [
        "standard",
        "outbuildings"
      ]
    },
    "lookupId": {
      "title": "Lookup ID",
      "description": "A unique identifier for a lookup.",
      "minLength": 1,
      "maxLength": 20,
      "type": "string",
      "examples": [
        "MH4",
        "ROFFICEA"
      ]
    },
    "lookupData": {
      "type": "object",
      "title": "Lookup data",
      "description": "Dictionary of `lookups` identified by `domain`.",
      "additionalProperties": {
        "$ref": "#/definitions/lookups"
      }
    },
    "lookups": {
      "type": "object",
      "title": "Lookups",
      "description": "Dictionary of `lookup` identified by `lookupId`.",
      "additionalProperties": {
        "$ref": "#/definitions/lookup"
      }
    },
    "lookup": {
      "type": "object",
      "title": "Lookup",
      "description": "Metadata and styles for association with elements of the sketch.",
      "properties": {
        "name": {
          "title": "Lookup short name",
          "description": "Relatively short name value that is commonly, although not always, used as part of the label text in sketches.",
          "type": "string",
          "minLength": 1,
          "maxLength": 50,
          "examples": [
            "RESIDENTIAL OFFICE"
          ]
        },
        "description": {
          "title": "Description",
          "description": "Description can be a significantly longer value than the `name`. It is not typically displayed in a sketch drawing but carries additional data for the CAMA system.",
          "minLength": 1,
          "maxLength": 255,
          "type": "string"
        },
        "ordinal": {
          "title": "Ordinal",
          "description": "Unique in a single `lookup` this is used for sorting the list of lookups in a client application",
          "minimum": 0,
          "type": "integer"
        },
        "styles": {
          "type": "object",
          "title": "Lookup styles",
          "description": "Styles are used to draw sketch elements correctly. The styles are linked to elements by their combined `lookupDataId`, `domain`, and `lookupId` keys for a `lookup`.",
          "properties": {
            "label": {
              "$ref": "#/definitions/style"
            },
            "dimensionLabel": {
              "$ref": "#/definitions/style"
            },
            "vector": {
              "$ref": "#/definitions/style"
            }
          },
          "additionalProperties": false
        },
        "attributes": {
          "title": "Lookup attributes",
          "description": "Any additional attributes that are needed for the `lookup`.",
          "oneOf": [
            {
              "$ref": "#/definitions/attributes"
            }
          ]
        }
      },
      "required": [
        "name"
      ],
      "additionalProperties": false
    },
    "areaType": {
      "title": "Area type",
      "description": "Vectors having positive area add their area to the calculated area of the segment to which they belong. Vectors having negative areas subtract from total calculated segment area.",
      "enum": [
        "positive",
        "negative"
      ],
      "default": "positive"
    },
    "drawingType": {
      "$comment": "There were three types previously: sketch, placeholder, and unsketched. We collapsed placeholder and unsketched into one concept.",
      "title": "Drawing type",
      "description": "A vector can be something that is sketched to represent a real-world shape. That is `sketched`. A vector can also be a `placeholder` for a real-world structure or segment of a structure. In that case the drawing type does not typically represent the real world thing, e.g., it's a symbol or a square. A `placeholder` may even have no visual representation at all beyond a label on the sketch.",
      "enum": [
        "sketched",
        "placeholder"
      ],
      "default": "sketched"
    },
    "creationMethod": {
      "title": "Derivation or creation method",
      "description": "The methodology used to create the data held in an attribute. The primary distinction is between data that is `computed` through a repeatable algorithm, and data that is manually `overridden` by a human. A typical case would be an area that is automatically calculated by a sketch application vs. a value for an area that is typed by a human because it is considered more relevant to the assessment process.",
      "enum": [
        "computed",
        "overridden"
      ],
      "default": "computed"
    },
    "scopedNumber": {
      "title": "Scoped value",
      "description": "A value such as area or perimeter that has both an explicit value and a record of how the value was derived.",
      "type": "object",
      "properties": {
        "value": {
          "type": "number"
        },
        "type": {
          "$ref": "#/definitions/creationMethod"
        }
      },
      "additionalProperties": false
    },
    "point-2d": {
      "title": "Two-dimensional point",
      "description": "A point with an location in two dimensional space. The units of the coordinate are unspecified, coming instead from the parent sketch.",
      "type": "object",
      "properties": {
        "x": {
          "type": "number"
        },
        "y": {
          "type": "number"
        }
      },
      "required": [
        "x",
        "y"
      ],
      "additionalProperties": false,
      "examples": [
        {
          "x": 0,
          "y": 0
        }
      ]
    },
    "rotation": {
      "title": "Rotation",
      "description": "Rotation with respect to the bearing of a line",
      "enum": [
        "clockwise",
        "counterclockwise"
      ],
      "default": "clockwise"
    },
    "line-2d": {
      "title": "Two-dimensional line",
      "description": "A line is defined--somewhat paradoxically--using a single 2-dimensional point, which is the _ending point_ of that line. The starting point of the line is either (a) the origin point of the sketch (typically `[0,0]`) or (b) the `origin` point for the vector containing the line, or (c) the endpoint of the line immediately proceeding the current line. In other words, if a line defines only it's end point then something else must be defining it's starting point, even if somewhat indirectly.",
      "type": "object",
      "properties": {
        "x": {
          "type": "number"
        },
        "y": {
          "type": "number"
        }
      },
      "required": [
        "x",
        "y"
      ],
      "additionalProperties": false
    },
    "arc-2d": {
      "title": "Two-dimensional arc",
      "description": "A simple two-point arc with a height and direction",
      "type": "object",
      "properties": {
        "h": {
          "title": "Height",
          "description": "Height of the arc",
          "type": "number"
        },
        "x": {
          "type": "number"
        },
        "y": {
          "type": "number"
        },
        "direction": {
          "$ref": "#/definitions/rotation",
          "default": "clockwise"
        }
      },
      "required": [
        "h",
        "x",
        "y"
      ],
      "additionalProperties": false
    },
    "ellipse-2d": {
      "title": "Two-dimensional ellipse",
      "description": "A simple ellipse with an x and y radius",
      "type": "object",
      "properties": {
        "rx": {
          "type": "number"
        },
        "ry": {
          "type": "number"
        }
      },
      "required": [
        "rx",
        "ry"
      ],
      "additionalProperties": false
    },
    "visibility": {
      "title": "Visibility",
      "description": "Showing or hiding something in a sketch is a visual treatment only. The element, such as a dimension or label, is still encoded in the SDS document. It is just not visible in a sketch that a user sees.",
      "enum": [
        "visible",
        "hidden"
      ],
      "default": "visible"
    },
    "dimensionLabel": {
      "type": "object",
      "title": "Dimension label",
      "description": "Drawing commands result in length values being calculated for edges. The appearance of dimension labels can be configured for vectors or individual commands.",
      "properties": {
        "padding": {
          "type": "number",
          "default": 1
        },
        "placement": {
          "$ref": "#/definitions/dimensionLabelPlacement"
        },
        "visibility": {
          "$ref": "#/definitions/visibility"
        },
        "style": {
          "$ref": "#/definitions/style"
        }
      },
      "additionalProperties": false
    },
    "styledLine": {
      "type": "object",
      "title": "line",
      "properties": {
        "line": {
          "$ref": "#/definitions/line-2d"
        },
        "style": {
          "$ref": "#/definitions/style"
        },
        "dimensionLabel": {
          "$ref": "#/definitions/dimensionLabel"
        }
      },
      "required": [
        "line"
      ],
      "additionalProperties": false
    },
    "styledArc": {
      "type": "object",
      "title": "arc",
      "properties": {
        "arc": {
          "$ref": "#/definitions/arc-2d"
        },
        "style": {
          "$ref": "#/definitions/style"
        },
        "dimensionLabel": {
          "$ref": "#/definitions/dimensionLabel"
        }
      },
      "required": [
        "arc"
      ],
      "additionalProperties": false
    },
    "styledEllipse": {
      "type": "object",
      "title": "ellipse",
      "properties": {
        "ellipse": {
          "$ref": "#/definitions/ellipse-2d"
        },
        "style": {
          "$ref": "#/definitions/style"
        },
        "dimensionLabel": {
          "$ref": "#/definitions/dimensionLabel"
        }
      },
      "required": [
        "ellipse"
      ],
      "additionalProperties": false
    },
    "area": {
      "title": "Area",
      "type": "object",
      "description": "Areas are alway positive because the way that they are used in calculations depends on their `areaType` rather than the sign (+/-) of their value.",
      "oneOf": [
        {
          "$ref": "#/definitions/scopedNumber"
        }
      ]
    },
    "perimeter": {
      "title": "Perimeter",
      "oneOf": [
        {
          "$ref": "#/definitions/scopedNumber"
        }
      ]
    },
    "ring": {
      "title": "Ring",
      "type": "object",
      "description": "A ring represents a drawing element.  Inner rings (`rings`) cannot overlap each other and must be contained within the drawing element defined by this ring's commands.",
      "properties": {
        "origin": {
          "title": "Origin",
          "description": "The origin point for a drawing command is very similar to the [move](https://developer.mozilla.org/en-US/docs/Web/SVG/Tutorial/Paths#line_commands) command in SVG: it's like positioning your pen on the page _ready_ to draw. So the `origin` is the first coordinate of any subsequent sequence of drawing commands.",
          "oneOf": [
            {
              "$ref": "#/definitions/point-2d"
            }
          ]
        },
        "commands": {
          "title": "Sketching commands",
          "description": "Following the spirit of standards like HTML Canvas and Scalable Vector Graphics (SVG), sketch commands describe how to draw elements on the page rather than describing the geometry itself. For example, rather than define a line as two coordinate pairs (start and end), the `line` command assumes an existing starting point, and its job is to 'draw a straight line to an end point`.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/command"
          }
        },
        "rings": {
          "title": "Rings",
          "description": "Inner rings to this ring.  If this ring is a non-hole, its inner rings represent holes.  If this ring is a hole, its inner rings represent non-holes.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/ring"
          }
        }
      },
      "required": [
        "origin",
        "commands"
      ],
      "additionalProperties": false
    },
    "command": {
      "title": "Sketching command",
      "oneOf": [
        {
          "$ref": "#/definitions/styledLine"
        },
        {
          "$ref": "#/definitions/styledArc"
        },
        {
          "$ref": "#/definitions/styledEllipse"
        }
      ]
    },
    "dimensionLabelPlacement": {
      "title": "Dimension label placement",
      "enum": [
        "inside",
        "outside"
      ],
      "default": "inside"
    },
    "labelPlacement": {
      "title": "Label placement",
      "enum": [
        "centered",
        "overridden"
      ],
      "default": "overridden"
    },
    "labelCollection": {
      "title": "Label collection",
      "type": "object",
      "properties": {
        "text": {
          "title": "Label text",
          "description": "Label text displayed on the sketch to annotate a segment or vector.",
          "type": "string"
        },
        "position": {
          "$ref": "#/definitions/point-2d"
        },
        "rotation": {
          "type": "number",
          "minimum": 0,
          "maximum": 360,
          "default": 0
        },
        "placement": {
          "$ref": "#/definitions/labelPlacement"
        },
        "values": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/labelValue"
          }
        },
        "style": {
          "$ref": "#/definitions/style"
        }
      },
      "required": [
        "text",
        "position",
        "values"
      ],
      "additionalProperties": false
    },
    "labelValue": {
      "type": "object",
      "title": "Label value",
      "properties": {
        "lookupName": {
          "title": "Lookup name",
          "description": "This should be a valid key present in the `lookupCollection` dictionary.",
          "oneOf": [
            {
              "$ref": "#/definitions/lookupDataId"
            }
          ]
        },
        "lookupCode": {
          "title": "Lookup code",
          "description": "This should be a valid key present in the `lookupCollection[lookupDataId][domain]` dictionary.",
          "oneOf": [
            {
              "$ref": "#/definitions/lookupId"
            }
          ]
        },
        "text": {
          "minLength": 1,
          "maxLength": 255,
          "type": "string"
        }
      },
      "required": [
        "lookupName",
        "lookupCode",
        "text"
      ],
      "additionalProperties": false
    },
    "vector": {
      "type": "object",
      "title": "Vector",
      "properties": {
        "id": {
          "type": "integer",
          "description": "Unique identifier of this vector with respect to all vectors of the document."
        },
        "label": {
          "$ref": "#/definitions/labelCollection"
        },
        "ring": {
          "title": "Outer ring",
          "description": "The outer ring of the vector.",
          "$ref": "#/definitions/ring"
        },
        "areaType": {
          "$ref": "#/definitions/areaType"
        },
        "drawingType": {
          "$ref": "#/definitions/drawingType"
        },
        "style": {
          "$ref": "#/definitions/style"
        },
        "dimensionLabel": {
          "$ref": "#/definitions/dimensionLabel"
        },
        "area": {
          "$ref": "#/definitions/area"
        },
        "perimeter": {
          "$ref": "#/definitions/perimeter"
        },
        "attributes": {
          "$ref": "#/definitions/attributes"
        }
      },
      "required": [
        "id",
        "ring"
      ],
      "additionalProperties": false
    },
    "page": {
      "description": "Page on which the item appears. The set of pages in a `sketch` is determined by collecting a list of unique page numbers in the child objects in that sketch. For example, if there are two values in a `sketch` of `segment[0].page = 1` and `segment[1].page = 4` then the `sketch` will have two pages with non-consecutive page numbers. The same can be true for `notes` in a `sketch`: they can in effect 'create' pages by referencing a page number.",
      "type": "integer",
      "minimum": 1
    },
    "segment": {
      "title": "Sketch segment",
      "description": "A segment is a logical piece of a sketch. It typically represents a segment of a structure that has characteristics affecting the assessed value of a property. For example, a segment might be a bedroom, or a patio, or an entire section of a large building. Segments are composed of vectors and carry their own summary area and perimeter",
      "type": "object",
      "properties": {
        "id": {
          "type": "integer",
          "description": "Unique identifier of this segment with respect to all segments of the document."
        },
        "label": {
          "$ref": "#/definitions/labelCollection"
        },
        "vectors": {
          "title": "Vectors",
          "description": "A segment is comprised of a collection or one or more vectors.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/vector"
          }
        },
        "boundarySize": {
          "description": "The boundary for a segment can optionally be set in 'ground units' (see `unitOfMeasure`). This has the effect of setting the square of ground within which the segment should be defined. For example, setting `boundarySize` to `50` if the `unitOfMeasure` are `feet` indicates that the segment should not extend outside of the 50x50 foot square with an origin of `[0,0]`.",
          "type": "number",
          "default": 100
        },
        "page": {
          "$ref": "#/definitions/page"
        },
        "area": {
          "$ref": "#/definitions/area"
        },
        "perimeter": {
          "$ref": "#/definitions/perimeter"
        },
        "domain": {
          "title": "Segment domain",
          "description": "A segment may represent an outbuilding. Used to access a `lookup` value for metadata and styling where the `segment.domain` (`domain`), `segment.label.values[].lookupName` (`lookupDataId`), and `segment.label.values[].lookupCode` (`lookupId`) values exist in `lookupCollection[lookupDataId][domain][lookupId]`.",
          "oneOf": [
            {
              "$ref": "#/definitions/domain"
            }
          ]
        }
      },
      "required": [
        "id"
      ],
      "additionalProperties": false
    },
    "note": {
      "title": "Sketch note",
      "description": "Notes are not associated with segments. They are free-form text positioned on a specific page of a sketch.",
      "type": "object",
      "properties": {
        "keycode": {
          "description": "Unique code or identifier representing the note within a sketch. By convention it takes the form 'page#/note#', e.g., '0/1' but it can be any string value.",
          "type": "string",
          "minLength": 1,
          "maxLength": 10
        },
        "text": {
          "description": "The label text visible to a user on the sketch surface.",
          "type": "string",
          "minLength": 1,
          "maxLength": 255
        },
        "page": {
          "$ref": "#/definitions/page"
        },
        "position": {
          "$ref": "#/definitions/point-2d"
        }
      },
      "required": [
        "keycode",
        "text",
        "page",
        "position"
      ],
      "additionalProperties": false
    },
    "sketch": {
      "title": "Appraisal sketch",
      "description": "A sketch is a collection of segments and related metadata that can be rendered as a standalone view of all--or part of--an assessable property. Depending on the capabilities of the CAMA system, there may but just one, or multiple sketches in an SDS document.",
      "type": "object",
      "properties": {
        "id": {
          "title": "Unique identifier",
          "description": "The sketch ID must be unique in the collection of sketches",
          "type": "integer"
        },
        "label": {
          "title": "Sketch label",
          "description": "The sketch label is a name for a sketch that a human can read and understand.",
          "type": "string",
          "minLength": 1,
          "maxLength": 50,
          "examples": [
            "Building #1"
          ]
        },
        "segments": {
          "title": "Sketch segments",
          "description": "A sketch has zero or more segments.",
          "type": "array",
          "items": {
            "$ref": "#/definitions/segment"
          },
          "minItems": 0
        },
        "notes": {
          "type": "array",
          "items": {
            "$ref": "#/definitions/note"
          }
        }
      },
      "required": [
        "id",
        "label",
        "segments"
      ],
      "additionalProperties": false
    }
  }
}