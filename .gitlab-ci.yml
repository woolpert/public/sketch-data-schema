variables:
  FILE_PREFIX: 'sketch-sds'

stages:
  - static analysis
  - test
  - build
  - deploy

default:
  image: alpine:3.12

unit test:
  stage: test
  extends: .testnodenv
  needs: [lint]
  script:
    - npm test
  artifacts:
    when: always
    expire_in: 30 days
    reports:
      junit: test/unit/report/junitresults.xml

lint:
  stage: static analysis
  extends: .testnodenv
  needs: []
  script:
    - npm run lint
  artifacts:
    when: always
    expire_in: 30 days
    reports:
      codequality: test/gl-codequality.json

generate schema:
  stage: build
  needs:
    - lint
    - unit test
  script:
    - rm -rf dist
    - mkdir -p ./dist
    - cp ./schema/*.schema.json ./dist
    - echo $CI_COMMIT_TAG > ./build-data/version.txt
    - SCHEMADOMAIN="$(cat ./build-data/domain.txt)\/$(cut -d'.' -f1 ./build-data/version.txt).$(cut -d'.' -f2 ./build-data/version.txt)"
    - IDSEARCH=$(cat ./build-data/idsearch.txt)
    - cd dist
    - find . -name "*.schema.json" -exec sed -i 's/'"$IDSEARCH"'/'"$IDSEARCH"''"$SCHEMADOMAIN"'\//g' '{}' \;
  artifacts:
    when: always
    expire_in: 1 hour
    paths:
      - dist

zip:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: ['']
  needs:
    - job: generate schema
      artifacts: true
    - job: pages
      artifacts: true
  script:
    - yum -y update && yum -y install zip
    - zip -rq build.zip dist public
  artifacts:
    name: build.zip
    when: always
    expire_in: 1 hour
    paths:
      - build.zip

latest:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: ['']
  needs:
    - job: zip
      artifacts: true
  script:
    - DATE_TIME=`date "+%Y%m%d%H%M%S"`
    - echo $DATE_TIME

    - FILE_NAME=$FILE_PREFIX-$DATE_TIME-$CI_COMMIT_SHORT_SHA${CI_COMMIT_TAG:+-$CI_COMMIT_TAG}.zip
    - echo $FILE_NAME

    - aws s3 cp build.zip s3://$AWS_ARTIFACTS_BUCKET/latest/$AWS_ARTIFACTS_PATH/$FILE_NAME
  except:
    - tags

release:
  stage: deploy
  image:
    name: amazon/aws-cli
    entrypoint: ['']
  needs:
    - job: zip
      artifacts: true
  script:
    - DATE_TIME=`date "+%Y%m%d%H%M%S"`
    - echo $DATE_TIME

    - FILE_NAME=$FILE_PREFIX-$DATE_TIME-$CI_COMMIT_SHORT_SHA${CI_COMMIT_TAG:+-$CI_COMMIT_TAG}.zip
    - echo $FILE_NAME

    - aws s3 cp build.zip s3://$AWS_ARTIFACTS_BUCKET/release/$AWS_ARTIFACTS_PATH/$FILE_NAME
  only:
    - tags

generate docs:
  stage: build
  image: python:3.8-slim-buster
  needs:
    - lint
    - unit test
  script:
    - apt update && apt-get -y install make
    - python -m pip install -q -r requirements.txt
    - make
  artifacts:
    paths:
      - output/
    expire_in: 1 hour

pages:
  stage: deploy
  when: always
  needs: [generate docs]
  script:
    - rm -Rf public/
    - mv output/ public/
    - mv index.html public/
  artifacts:
    paths:
      - public
    expire_in: 1 hour

.testnodenv:
  image: node:14-alpine
  before_script:
    - cd test/
    - npm install
