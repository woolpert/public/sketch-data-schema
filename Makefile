.PHONY: docs

docs:
	python3 docs/schema_docs.py --schema data

clean:
	rm -rf output