import Ajv2020, { ValidateFunction } from "ajv/dist/2020";
import schema from '../../../schema/data.schema.json';

describe("Data Schema", () => {

    let validator: ValidateFunction<unknown>;

    beforeAll(() => {
        const ajv = new Ajv2020({
            schemas: [],
            allowUnionTypes: true
        });
        validator = ajv.compile(schema);
    });

    it("Compiles", () => {

        expect(validator).toBeDefined();
    });
});