import { readdir, readFile } from 'fs/promises';
import * as path from 'path';

interface SemVer {
  name: string;
  major: number;
  minor: number;
}

const documentsDir = path.join(path.resolve(__dirname, '../'), 'test-data', 'documents');

export async function loadDocument(name: string): Promise<unknown> {
  const text = (await readFile(path.join(documentsDir, name), { encoding: 'utf-8' })).toString();
  return JSON.parse(text);
}

/**
 * Parse a sem ver file name (e.g, foo-2.1.0.json)
 */
function parseSemVer(name: string): SemVer | null {
  const results = /.*(\d+)\.(\d+)\.\d+\.json$/i.exec(name);
  if (results === null) {
    return null;
  }
  return { name, major: +results[1], minor: +results[2] };
}

/**
 * Gets the set of document names for the latest semantic version
 */
export async function getLatestDocuments(): Promise<string[]> {
  const documents: string[] = [];
  // Find the semantic versioned files, then sort descending
  const semVers = (await readdir(documentsDir, { withFileTypes: true }))
    .filter(d => d.isFile())
    .map(d => parseSemVer(d.name) as unknown as SemVer)
    .filter(x => x !== null)
    .sort((a, b) => (b.major - a.major) || (b.minor - a.minor));

  // Take the latest semantic versioned set of files
  const { major, minor } = semVers[0] || {};
  for (const semVer of semVers) {
    if (semVer.major !== major || semVer.minor !== minor) {
      break;
    }
    documents.push(semVer.name);
  }
  return documents;
}
