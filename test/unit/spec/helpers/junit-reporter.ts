import { JUnitXmlReporter } from 'jasmine-reporters';

const junitReporter = new JUnitXmlReporter({
  savePath: `${__dirname}/../../report`,
  consolidateAll: true
});
jasmine.getEnv().addReporter(junitReporter);
