import Ajv2020, { ValidateFunction } from "ajv/dist/2020";
import schema from '../../../schema/data.schema.json';
import { getLatestDocuments, loadDocument } from "./helpers/document";
import invalidDocument from './test-data/documents/invalid.json';

describe('Document', () => {

  let validDocuments: string[];
  let validator: ValidateFunction<unknown>;

  beforeAll(async () => {
    validDocuments = await getLatestDocuments();
  });

  beforeEach(() => {
    const ajv = new Ajv2020({
      schemas: [],
      allowUnionTypes: true,
    });
    validator = ajv.compile(schema);
  });

  it('Latest semantic version documents pass validation', async () => {
    for (const name of validDocuments) {
      const document = await loadDocument(name);

      const result = validator(document);

      expect(result).withContext(name).toBeTrue();
      expect(validator.errors).withContext(name).toBeFalsy();
    }
  });

  it('Invalid document fails validation', async () => {
    const document = invalidDocument;

    const result = validator(document);

    expect(result).toBeFalse();
    expect(validator.errors).toBeDefined();
  });

});
