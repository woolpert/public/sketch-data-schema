# Sketch Data Schema

**Sketch Data Schema** (SDS) is an open interchange format that represents vector data for Computer-Aided Mass Appraisal (CAMA) systems.

## Learn

Read the [SDS standard](https://opencamadata.org).

## Implementations

- [CAMA Cloud Sketcher (Data Cloud Solutions)](https://sketcher.camacloud.com)

## License

See [LICENSE](LICENSE).
